# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Zain_Product(models.Model):
    _name = 'zain.product'
    _rec_name = 'code'
    _description = 'add product'

    code = fields.Char(string="الكود", required=True, )
    name = fields.Char(string="الاسم", required=True, )
    amount = fields.Integer(string="الكمية", required=True, )
    cost = fields.Float(string="التكلفة", required=True, )
    date = fields.Date(string="التاريخ", required=True, )
    total_amount = fields.Float(string="الا جمالي",  required=False, )

    @api.onchange('amount')
    def total_purchas(self):
        self.total_amount = self.cost*self.amount


    @api.multi
    def change_update_date(self):
        self.ensure_one()


    def update_record(self):
        self.ensure_one()
        all_record=self.env['zain.product'].search([])

        for record in all_record:
            prod_amount =self.amount+record.amount
            self.update({
                'amount':prod_amount,
                'cost':self.cost,
                'total_amount':self.cost*prod_amount
            })





class ProductSale(models.Model):
    _name = 'zain.product_sale'
    _rec_name = 'name'
    _description = 'New Description'

    product_sale_id = fields.Many2one(comodel_name="zain.product", string="", required=False, )
    name = fields.Char()
    price=fields.Float()
    amount=fields.Integer()
    total_amount=fields.Float()
    date = fields.Date(string="", required=False, )

    @api.onchange('product_sale_id')
    def product_name(self):
        self.name = self.product_sale_id.name
    @api.onchange('amount')
    def sell_update_amount(self):
        self.total_amount=self.price*self.amount

    def update_record_sale(self):
        self.ensure_one()
        all_record = self.env['zain.product']
        am_sale=self.product_sale_id.amount - self.amount
        for record in self:
            all_record=self.env['zain.product'].search([('code','=',record.product_sale_id.code)])
        all_record.write({
            'amount': am_sale,
            'total_amount':am_sale*self.product_sale_id.cost

                          })




class ZainCategory(models.Model):
    _name = 'zain.category_product'
    _rec_name = 'name'
    _description = 'New Description'

    name = fields.Char()
    nots =fields.Char(sring='nots')


